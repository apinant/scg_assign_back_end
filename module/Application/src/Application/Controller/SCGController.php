<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class SCGController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'index');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);
        return $view;       
    } 
################################################################################
    public function indexAction() 
    {
        try
        {
            $view = $this->basic();
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function xyzAction() 
    {
        try
        {
            // Case input text
            $str = "3, 5, 9, 15, x, y, z";
            $arr = array_map('trim', explode(',', $str));

            // First Value.
            // $t[1] = 3;
            $t[1] = $arr[0];
            // Array Count.
            // $n = 7;
            $n = count($arr);
            if($n>1){
                for ($i=2; $i <= $n; $i++) {
                    $t[] = $t[$i-1] + (2*($i-1));
                }
            }
            // echo "<pre>";
            // print_r($t);
            // echo "</pre>";
            
            $view = $this->basic();
            $view->input_str = $str;
            $view->data = $t;
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function placeAction()
    {
        try
        {
            // GOOGLE API KEY FOR SCG ASSIGNMENT.
            $API_KEY = "AIzaSyAfeeCE3_aoTZzh28i5k-YT71qrBjfX-jE";
            $search = "restaurants+in+Bangsue";
            $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".$search."&key=".$API_KEY;
            $json = file_get_contents($url);
            header("Content-Type: application/json; charset=utf-8");
            print_r($json);
            exit(0);
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function mapsAction()
    {
        try
        {
            // GOOGLE API KEY FOR SCG ASSIGNMENT.
            $API_KEY = "AIzaSyAfeeCE3_aoTZzh28i5k-YT71qrBjfX-jE";
            $search = "restaurants+in+Bangsue";
            $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".$search."&key=".$API_KEY;            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = curl_exec($ch);
            curl_close($ch);
            header("Content-Type: application/json; charset=utf-8");
            print_r($data);
            exit();
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

}