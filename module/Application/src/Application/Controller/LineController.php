<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Application\Models\Food;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class LineController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'index');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);
        return $view;       
    }
################################################################################
    public function testAction()
    {
        $view = $this->basic();
        $models = new Food($this->adapter, $view->id, $view->page);
        $x = $models->getList();
        print_r($x);
        exit();
    }
################################################################################
    public function indexAction()
    {
        try
        {
            $view = $this->basic();

            // ACCESS TOKEN FOR SCG ASSIGNMENT ONLY.
            $accessToken = "Ov9A8z9sJylI/VxyjQZUE5N2N+eU0A014rXBM6wqk7l9MUbLpw1VGFcESvbWuTu9oXnHG3GY5EBtJ6sQcDo7Y3eHr/57k6/PEd55nJGv378Eu8iT4RIVYevaYZnadMRAN6Ov7VK8BOM7CRW6nrWnSQdB04t89/1O/w1cDnyilFU=";//copy Channel access token ตอนที่ตั้งค่ามาใส่
    
            $content = file_get_contents('php://input');
            $arrayJson = json_decode($content, true);
            
            $arrayHeader = array();
            $arrayHeader[] = "Content-Type: application/json";
            $arrayHeader[] = "Authorization: Bearer {$accessToken}";
            
            //Get message from user.
            $message = $arrayJson['events'][0]['message']['text'];
            // Message Type "Text"
            if($message == "สวัสดี"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "text";
                $arrayPostData['messages'][0]['text'] = "สวัสดีครับ";
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Sticker"
            else if($message == "ฝันดี"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "sticker";
                $arrayPostData['messages'][0]['packageId'] = "11537";
                $arrayPostData['messages'][0]['stickerId'] = "52002764";
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Image"
            else if($message == "แมว"){
                $image_url = "https://placekitten.com/300/300";
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "image";
                $arrayPostData['messages'][0]['originalContentUrl'] = $image_url;
                $arrayPostData['messages'][0]['previewImageUrl'] = $image_url;
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Location"
            else if($message == "พิกัดบางซ่อน"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "location";
                $arrayPostData['messages'][0]['title'] = "บางซ่อน";
                $arrayPostData['messages'][0]['address'] =   "13.82020292736143,100.53252518177032";
                $arrayPostData['messages'][0]['latitude'] = "13.82020292736143";
                $arrayPostData['messages'][0]['longitude'] = "100.53252518177032";
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Text + Sticker ใน 1 ครั้ง"
            else if($message == "ลาก่อน"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "text";
                $arrayPostData['messages'][0]['text'] = "แล้วเจอกันใหม่";
                $arrayPostData['messages'][1]['type'] = "sticker";
                $arrayPostData['messages'][1]['packageId'] = "1";
                $arrayPostData['messages'][1]['stickerId'] = "131";
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Video"
            else if($message == "video"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "video";
                $arrayPostData['messages'][0]['originalContentUrl'] = "https://mokmoon.com/videos/Brown.mp4";//ใส่ url ของ video ที่ต้องการส่ง
                $arrayPostData['messages'][0]['previewImageUrl'] = "https://linefriends.com/img/bangolufsen/img_og.jpg";//ใส่รูป preview ของ video
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            // Message Type "Audio"
            else if($message == "audio"){
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "audio";
                $arrayPostData['messages'][0]['originalContentUrl'] = "https://mokmoon.com/audios/line.mp3";//ใส่ url ของ audio ที่ต้องการส่ง
                $arrayPostData['messages'][0]['duration'] = 1000; // ความยาวของไฟล์เสียง หน่วยเป็น milliseconds
                $this->replyMsg($arrayHeader,$arrayPostData);
            }else{
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "text";
                $arrayPostData['messages'][0]['text'] = "สวัสดีครับ ต้องการให้ช่วยอะไรครับ?";
                $arrayPostData['messages'][1]['type'] = "sticker";
                $arrayPostData['messages'][1]['packageId'] = "1";
                $arrayPostData['messages'][1]['stickerId'] = "134";
                $this->replyMsg($arrayHeader,$arrayPostData);
            }
            exit();
            // $view = $this->basic();
            // return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function replyMsg($arrayHeader,$arrayPostData)
    {
        $strUrl = "https://api.line.me/v2/bot/message/reply";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$strUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arrayHeader);    
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arrayPostData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close ($ch);
    }
################################################################################
    public function pushMsg($arrayHeader,$arrayPostData)
    {
        $strUrl = "https://api.line.me/v2/bot/message/push";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$strUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arrayHeader);    
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arrayPostData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close ($ch);
    }
}